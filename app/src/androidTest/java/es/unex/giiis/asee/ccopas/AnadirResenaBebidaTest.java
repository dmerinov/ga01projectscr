package es.unex.giiis.asee.ccopas;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giiis.asee.ccopas.uis.BarManagerActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AnadirResenaBebidaTest {

    @Rule
    public ActivityTestRule<BarManagerActivity> mActivityTestRule = new ActivityTestRule<>(BarManagerActivity.class);

    @Test
    public void anadirResenaBebidaTest() {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.bar_reclycler_view),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction tabView = onView(
                allOf(withContentDescription("Carta"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.tabs),
                                        0),
                                1),
                        isDisplayed()));
        tabView.perform(click());

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.listaCartaBarRV),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction tabView2 = onView(
                allOf(withContentDescription("Reseñas"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.tabs_bebida),
                                        0),
                                1),
                        isDisplayed()));
        tabView2.perform(click());

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.add_review_fab), withContentDescription("Añadir reseña"),
                        childAtPosition(
                                allOf(withId(R.id.review_bar),
                                        withParent(withId(R.id.view_pager_bebida))),
                                1),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.autor),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.coordinatorlayout.widget.CoordinatorLayout")),
                                        1),
                                1),
                        isDisplayed()));
        appCompatEditText.perform(replaceText("Prueba"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.title),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.coordinatorlayout.widget.CoordinatorLayout")),
                                        1),
                                3),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("Titulo"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.text),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.coordinatorlayout.widget.CoordinatorLayout")),
                                        1),
                                5),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("Comentario"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.submitButton), withText("Añadir"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("androidx.coordinatorlayout.widget.CoordinatorLayout")),
                                        1),
                                10),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.user_review), withText("Prueba"),
                        withParent(withParent(withId(R.id.bar_review_recycler_view))),
                        isDisplayed()));
        textView.check(matches(withText("Prueba")));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.title_review), withText("Titulo"),
                        withParent(withParent(withId(R.id.bar_review_recycler_view))),
                        isDisplayed()));
        textView2.check(matches(withText("Titulo")));

        ViewInteraction ratingBar = onView(
                allOf(withId(R.id.ratingBar),
                        withParent(withParent(withId(R.id.bar_review_recycler_view))),
                        isDisplayed()));
        ratingBar.check(matches(isDisplayed()));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.text_review), withText("Comentario"),
                        withParent(withParent(withId(R.id.bar_review_recycler_view))),
                        isDisplayed()));
        textView3.check(matches(withText("Comentario")));

        ViewInteraction imageButton = onView(
                allOf(withId(R.id.add_review_fab), withContentDescription("Añadir reseña"),
                        withParent(allOf(withId(R.id.review_bar),
                                withParent(withId(R.id.view_pager_bebida)))),
                        isDisplayed()));
        imageButton.check(matches(isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
