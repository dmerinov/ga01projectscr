package es.unex.giiis.asee.ccopas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.R;

public class bebidasAdapter extends RecyclerView.Adapter<bebidasAdapter.MyViewHolder> {

    private Context mContext;
    private List<Bebida> drink_list = new ArrayList<>();
    private final OnItemClickListener listener;

    private FragmentActivity fragmentActivity;

    public bebidasAdapter(Context mContext, FragmentActivity fragmentActivity, OnItemClickListener listener) {
        this.mContext = mContext;
        this.fragmentActivity = fragmentActivity;
        this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.bebida_item, parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(drink_list.get(position),listener);
    }

    public void addAll(ArrayList<Bebida> bebidasActualizadas) {
        drink_list.clear();
        drink_list.addAll(bebidasActualizadas);
        notifyDataSetChanged();
    }

    public void add(Bebida bebida) {
        drink_list.add(bebida);
        notifyDataSetChanged();
    }

    public interface OnItemClickListener{
        void onItemClick(Bebida bebida);
    }

    @Override
    public int getItemCount() {
        return drink_list.size();
    }

    public void setFilters(ArrayList<Bebida> listBebidas) {
        drink_list.clear();
        drink_list.addAll(listBebidas);
        notifyDataSetChanged();
    }
    public static class  MyViewHolder extends RecyclerView.ViewHolder {
        //Los adaptadores de Recycler view deben contener una clase que extienda de viewHolder
        TextView drink_name;
        TextView drink_type;
        public MyViewHolder(@NonNull View view) {
            super(view);
            drink_name = view.findViewById(R.id.drink_nameTV);
        }

        public void bind(final Bebida bebida, final bebidasAdapter.OnItemClickListener listener){
            drink_name.setText(bebida.getNombre());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        listener.onItemClick(bebida);
                }
            });
        }

    }
}
