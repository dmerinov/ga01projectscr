package es.unex.giiis.asee.ccopas.uis;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import es.unex.giiis.asee.ccopas.DetalleBebidas.SectionsPagerBebidaAdapter;
import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.model.Bebida;

public class DetalleBebidaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_bebida);

        Bebida bebida= (Bebida) getIntent().getSerializableExtra("InstanciaBebida");
        manageTabs(bebida);
    }
    private void manageTabs(Bebida bebida){
        SectionsPagerBebidaAdapter sectionBebida = new SectionsPagerBebidaAdapter(this, getSupportFragmentManager(), bebida);
        ViewPager viewPager = findViewById(R.id.view_pager_bebida);
        viewPager.setAdapter(sectionBebida);
        TabLayout tabs = findViewById(R.id.tabs_bebida);
        tabs.setupWithViewPager(viewPager);
    }
}