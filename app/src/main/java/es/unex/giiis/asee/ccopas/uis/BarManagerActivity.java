package es.unex.giiis.asee.ccopas.uis;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.API.BarNetworkLoaderRunnable;
import es.unex.giiis.asee.ccopas.API.NetworkingAndroidHttpClientJSON;
import es.unex.giiis.asee.ccopas.DrawerLayoutFragments.AboutFragment;
import es.unex.giiis.asee.ccopas.DrawerLayoutFragments.BarListFragment;
import es.unex.giiis.asee.ccopas.DrawerLayoutFragments.Bebidas_favoritas_fragment;
import es.unex.giiis.asee.ccopas.DrawerLayoutFragments.FavBarListFragment;
import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.adapters.BarListAdapter;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.BarOpenData;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.model.Binding;
import es.unex.giiis.asee.ccopas.model.Results;
import es.unex.giiis.asee.ccopas.model.Review;
import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;

public class BarManagerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        DrawerLayout.DrawerListener {


    private DrawerLayout drawerLayout;
    private BarListFragment barListFragment;
    private FavBarListFragment favBarListFragment;
    private AboutFragment aboutFragment;
    private Bebidas_favoritas_fragment fav_drinks;
    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        fragmentManager= getSupportFragmentManager();
        Toolbar toolbar = findViewById(R.id.bar_manager_toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);


        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);


        if(barListFragment==null) {
            int title = R.string.main_bar_list;
            BarListFragment fragment = BarListFragment.newInstance(getString(title));
            barListFragment= fragment;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
        }
    }




    public boolean buscarNombreBar(ArrayList<Bar> bares, String nombre){
        boolean success=false;
        int i = 0;
        Bar bar;
        while(!success&& i<bares.size()){
            bar=bares.get(i);
            i++;
            if(bar.getNombre().equals(nombre)){
                success=true;
            }
        }
        return success;
    }
    public boolean buscarNombreBebida(ArrayList<Bebida> bebidas, String nombre){
        boolean success=false;
        int i = 0;
        Bebida bebida;
        while(!success&& i<bebidas.size()){
            bebida=bebidas.get(i);
            i++;
            if(bebida.getNombre().equals(nombre)){
                success=true;
            }
        }
        return success;
    }


    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int title;
        switch (item.getItemId()) {
            case R.id.nav_bar_list:
                title = R.string.main_bar_list;
                if(barListFragment==null){
                    BarListFragment fragment = BarListFragment.newInstance(getString(title));
                    barListFragment=fragment;
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commit();
                }
                else{
                    BarListFragment fragment = barListFragment;
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commit();
                }
                break;
            case R.id.nav_fav_bar:
                title = R.string.fav_bar_list;
                if(favBarListFragment==null){
                    FavBarListFragment fragment = FavBarListFragment.newInstance(getString(title));
                    favBarListFragment=fragment;
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commit();
                }
                else{
                    FavBarListFragment fragment=favBarListFragment;
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commit();
                }
                setTitle(getString(title));
                break;
            case R.id.nav_fav_drink:
                title = R.string.fav_drink_list;
                if(fav_drinks==null){
                    Bebidas_favoritas_fragment fragment = Bebidas_favoritas_fragment.newInstance(getString(title));
                    fav_drinks =fragment;
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commit();
                }
                else{
                    Bebidas_favoritas_fragment fragment=fav_drinks;
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commit();
                }
                setTitle(getString(title));
                break;
            case R.id.nav_about:
                title = R.string.about;

                if(aboutFragment==null){
                    AboutFragment fragment = AboutFragment.newInstance();
                    aboutFragment=fragment;
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commit();


                }
                else{
                    AboutFragment fragment=aboutFragment;
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.fragment_container,fragment).commit();
                }
                setTitle(getString(title));
                break;

            default:
                throw new IllegalArgumentException("opción del menú no implementada");

        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {

    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
