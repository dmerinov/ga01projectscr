package es.unex.giiis.asee.ccopas.ui.tabsDetalleBar;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import es.unex.giiis.asee.ccopas.InjectorUtils;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.uis.AddReviewActivity;
import es.unex.giiis.asee.ccopas.BarNetworkDataSource;
import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.BebidaNetworkDataSource;
import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.adapters.ReviewListAdapter;
import es.unex.giiis.asee.ccopas.model.Review;
import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BebidasFavoritasFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BebidasFavoritasFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.ReviewFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.ReviewFragmentViewModelFactory;

import static android.app.Activity.RESULT_OK;

/**
 * A placeholder fragment containing a simple view.
 */
public class ReviewFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String BAR_LIST = "barList";
    private String nombreBar;
    private static final int ADD_TODO_ITEM_REQUEST = 0;
    private Bar bar;
    private RecyclerView reviewBarRecyclerView;
    private ArrayList<Review> reviewList;
    private ReviewListAdapter reviewListAdapter;
    private ReviewFragmentViewModel mViewModel;

    public ReviewFragment() {
    }

    public static ReviewFragment newInstance(int index, Bar bar) {
        ReviewFragment fragment = new ReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);

        if (bar != null)
            bundle.putSerializable(BAR_LIST, bar);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewModelProvider.NewInstanceFactory view = new ViewModelProvider.NewInstanceFactory();
        view.create(PageViewModel.class);
        PageViewModel pageViewModel = new ViewModelProvider(this, view).get(PageViewModel.class);

        reviewList=new ArrayList<>();
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            bar = (Bar) getArguments().getSerializable(BAR_LIST);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_review_bar, container, false);
        ReviewFragmentViewModelFactory factory = InjectorUtils.provideReviewFragmentViewModelFactory(this.getActivity().getApplicationContext(),bar.getNombre());
        mViewModel = new ViewModelProvider(this,factory).get(ReviewFragmentViewModel.class);
        reviewBarRecyclerView = root.findViewById(R.id.bar_review_recycler_view);

        reviewBarRecyclerView.setHasFixedSize(true);
        reviewBarRecyclerView.addItemDecoration(new DividerItemDecoration(root.getContext(), DividerItemDecoration.VERTICAL));
        reviewBarRecyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        reviewListAdapter = new ReviewListAdapter(root.getContext(), review -> {

        });

        FloatingActionButton fab = root.findViewById(R.id.add_review_fab);
        fab.setOnClickListener(view -> {
            requireActivity().getApplicationContext();
            Intent addReviewIntent = new Intent(view.getContext(), AddReviewActivity.class);
            startActivityForResult(addReviewIntent, ADD_TODO_ITEM_REQUEST);
        });

        reviewBarRecyclerView.setAdapter(reviewListAdapter);

        mViewModel.getmReviews().observe(getViewLifecycleOwner(), reviews -> onReviewsLoaded((ArrayList<Review>)reviews));


        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_TODO_ITEM_REQUEST) {
            if (resultCode == RESULT_OK) {
                Review review = new Review(data,bar.getNombre());
                review.setTipo(Review.TipoReview.Bar);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mViewModel.insertarReview(review);
                    }
                });

            }
        }

    }
    private void onReviewsLoaded(ArrayList<Review> reviews) {
        getActivity().runOnUiThread(() -> reviewListAdapter.addAll(reviews));
    }
}