package es.unex.giiis.asee.ccopas;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import es.unex.giiis.asee.ccopas.API.BarNetworkLoaderRunnable;
import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.Binding;

public class BarNetworkDataSource {
    private static BarNetworkDataSource mInstance;

    private final MutableLiveData<ArrayList<Bar>> mDownloadedBares = new MutableLiveData<ArrayList<Bar>>();
    private final ArrayList<Bar> bares = new ArrayList<>();


    public synchronized static BarNetworkDataSource getInstance(){
        if(mInstance == null){
            mInstance = new BarNetworkDataSource();
        }
        return  mInstance;
    }
    public LiveData<ArrayList<Bar>> getCurrentBar(){
        return mDownloadedBares;
    }
    public ArrayList<Bar> getCurrentBarList(){
        return bares;
    }
    public void fetchBares(){
        AppExecutors.getInstance().networkIO().execute(new BarNetworkLoaderRunnable((barOD) ->{
            ArrayList<Bar> baresAux = new ArrayList<>();
            Bar bar;
            for(Binding bind : barOD.getResults().getBindings()){
                bar = new Bar(bind);
                baresAux.add(bar);
            }
            System.out.println(baresAux.size());
            mDownloadedBares.postValue(baresAux);
        }));
    }
}
