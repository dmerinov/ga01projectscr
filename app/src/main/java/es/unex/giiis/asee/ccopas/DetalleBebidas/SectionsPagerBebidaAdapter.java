package es.unex.giiis.asee.ccopas.DetalleBebidas;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.model.Bebida;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerBebidaAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[] {R.string.bebida_tab_text_1, R.string.bebida_tab_text_2};
    private final Fragment[] tabs;
    private final Context mContext;
    private Bebida bebida;

    public SectionsPagerBebidaAdapter(Context context, FragmentManager fm, Bebida bebida) {
        super(fm);
        mContext = context;
        tabs= new Fragment[2];
        this.bebida = bebida;
    }

    /**
     *   Is called to instantiate the fragment for the given page
     * @param position
     * @return a PlaceholderFragment (defined as a static inner class below)
     */
    @NonNull
    @Override
    public Fragment getItem(int position) {
        tabs[position] = PlaceholderBebidasFragment.newInstance(position,bebida);
        return tabs[position];
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return TAB_TITLES.length;
    }
}