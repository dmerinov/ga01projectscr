package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.model.Review;

public class PlaceholderBebidasFragmentViewModel extends ViewModel {
    private final BarRepository barRepository;
    private final LiveData<List<Review>> mReviews;

    public PlaceholderBebidasFragmentViewModel(BarRepository barRepository,String nombre){
        this.barRepository=barRepository;
        mReviews = barRepository.obtenerReviews(nombre, Review.TipoReview.Bebida);
    }

    public LiveData<List<Review>> getmReviews(){
        return mReviews;
    }
    public void insertarReview(Review review){
        barRepository.insertarReview(review);
    }
    public void actualizarBebida(Bebida bebida) { barRepository.actualizarBebida(bebida);}
}
