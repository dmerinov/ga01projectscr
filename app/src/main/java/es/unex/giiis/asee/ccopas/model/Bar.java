package es.unex.giiis.asee.ccopas.model;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.roomdb.Converter.BebidasListConverter;
import es.unex.giiis.asee.ccopas.roomdb.Converter.ReviewListConverter;

@Entity(tableName = "bares")
public class Bar implements Serializable {
    @PrimaryKey @NonNull
    private String nombre;
    @ColumnInfo
    private String telefono;
    @ColumnInfo
    private String direccion;
    @ColumnInfo
    private int main_photo;
    @ColumnInfo
    private boolean fav;
    @Ignore
    public final static String NAME = "nombreBar";
    @Ignore
    public final static String DIR = "direccion";
    @Ignore
    public final static String AFORO = "aforo";
    @Ignore
    public final static String TELF = "telefono";

    @ColumnInfo
    private String aforo;
    @TypeConverters(BebidasListConverter.class)
    private List<Bebida> bebidasBar = new ArrayList<>();
    @TypeConverters(ReviewListConverter.class)
    private List<Review> barReviews = new ArrayList<>();

    public static void packageIntent(Intent data, String mNombreBar, String mDireccion, String mAforo, String mTelefono) {
        data.putExtra(Bar.NAME,mNombreBar);
        data.putExtra(Bar.DIR,mDireccion);
        data.putExtra(Bar.AFORO,mAforo);
        data.putExtra(Bar.TELF,mTelefono);
    }
    @Ignore
    public Bar(Intent intent){
        nombre=intent.getStringExtra(Bar.NAME);
        direccion = intent.getStringExtra(Bar.DIR);
        aforo = intent.getStringExtra(Bar.AFORO);
        telefono = intent.getStringExtra(Bar.TELF);
        main_photo = R.drawable.bar_icon;
    }

    public List<Review> getBarReviews() {
        return barReviews;
    }

    public void setBarReviews(List<Review> barReviews) {
        if(barReviews==null){
            barReviews= new ArrayList<>();
        }
        this.barReviews.addAll(barReviews);
    }

    public List<Bebida> getBebidasBar(){return bebidasBar;}

    public void setBebidasBar(List<Bebida> ListaNueva){
        if(bebidasBar==null){
            bebidasBar= new ArrayList<>();
        }
        bebidasBar.addAll(ListaNueva);

    }

    public boolean isFav() {
        return fav;
    }

    public void setFav(boolean fav) {
        this.fav = fav;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getMain_photo() {
        return main_photo;
    }

    public void setMain_photo(int main_photo) {
        this.main_photo = main_photo;
    }

    public String getAforo() {
        return aforo;
    }

    public void setAforo(String aforo) {
        this.aforo = aforo;
    }

    public Bar(String nombre, String telefono, String direccion, int main_photo, boolean fav, String aforo, List<Bebida> bebidasBar, List<Review> barReviews) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.main_photo = main_photo;
        this.fav = fav;
        this.aforo = aforo;
        this.bebidasBar = bebidasBar;
        this.barReviews = barReviews;
    }
    @Ignore
    public Bar(String nombre, String telefono, String direccion, int main_photo, boolean fav, String aforo) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.main_photo = main_photo;
        this.fav = fav;
        this.aforo = aforo;
        this.bebidasBar = new ArrayList<>();
        this.barReviews = new ArrayList<Review>();
    }
    @Ignore
    public Bar(){
        // Constructor required but not used
        this.nombre = "default";

    }

    // Constructor parameterized with the JSON data
    @Ignore
    public Bar(Binding bind) {
        String aforo;
        String telefono;

        // The identifier is not necessary to put it because the database is already in charge
        this.nombre = bind.getRdfsLabel().getValue();
        if(bind.getSchemaTelephone() != null){
            telefono = bind.getSchemaTelephone().getValue();
            if(telefono.contains("-"))
                this.telefono = telefono.substring(0, telefono.lastIndexOf("-"));
            else
                this.telefono=telefono;
        }else{
            this.telefono = "0";
        }
        this.direccion = (bind.getSchemaAddressStreetAddress() != null) ? bind.getSchemaAddressStreetAddress().getValue() : "0";
        this.main_photo = R.drawable.bar_icon;
        this.fav = false;
        if(bind.getOmCapacidadPersonas() != null){
            aforo = bind.getOmCapacidadPersonas().getValue();
            this.aforo = aforo.substring(0, aforo.lastIndexOf(" "));
        }else{
            this.aforo = "0";
        }
        this.barReviews= new ArrayList<Review>();
    }


    @Override
    public String toString(){
        return getNombre() + "\n " + getTelefono() + "\n " + getDireccion() + "\n " + getAforo() + "\n\n \n  ";
    }
}
