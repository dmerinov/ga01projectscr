package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.ccopas.BarRepository;

public class BarListFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final BarRepository barRepository;

    public BarListFragmentViewModelFactory(BarRepository barRepository){
        this.barRepository = barRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new BarListFragmentViewModel(barRepository);
    }
}
