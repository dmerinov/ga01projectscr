package es.unex.giiis.asee.ccopas.roomdb.Converter;

import androidx.room.TypeConverter;

import java.util.Date;

public class DateConverter {
    @TypeConverter
    public Date timestampToDate(Long timestamp){
        return timestamp == null ? null : new Date(timestamp);
    }
    @TypeConverter
    public Long dateToTimestamp(Date date){
        return date==null ? null : date.getTime();
    }
}
