package es.unex.giiis.asee.ccopas;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.BarOpenData;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.model.Review;
import es.unex.giiis.asee.ccopas.roomdb.Dao.BarDao;
import es.unex.giiis.asee.ccopas.API.NetworkingAndroidHttpClientJSON;
import es.unex.giiis.asee.ccopas.roomdb.Dao.BebidaDao;
import es.unex.giiis.asee.ccopas.roomdb.Dao.ReviewDao;

public class BarRepository {
    private static final String LOG_TAG = BarRepository.class.getSimpleName();
    private static BarRepository mInstance;
    private final BarDao mBarDao;
    private final BebidaDao mBebidaDao;
    private final ReviewDao mReviewDao;
    private final BarNetworkDataSource mBarNetworkDataSource;
    private final BebidaNetworkDataSource mBebidaNetworkDatasource;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private MutableLiveData<List<Bar>> bares = new MutableLiveData<List<Bar>>();
    private final MutableLiveData<ArrayList<Bebida>> bebida = new MutableLiveData<>();
    private final Map<String,Long> lastUpdateTimeMillisMap = new HashMap<>();
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 30000;
    private final BarOpenData barOD = new BarOpenData();
    private LiveData<ArrayList<Bebida>> bebidasCocktailDB;


    private BarRepository(BarDao barDao, BarNetworkDataSource barNetworkDataSource,BebidaDao bebidaDao, BebidaNetworkDataSource bebidaNetworkDataSource, ReviewDao reviewDao){
        mBarDao= barDao;
        mReviewDao=reviewDao;
        mBebidaDao=bebidaDao;
        mBarNetworkDataSource= barNetworkDataSource;
        mBebidaNetworkDatasource=bebidaNetworkDataSource;
        mBarNetworkDataSource.fetchBares();
        mBebidaNetworkDatasource.fetchBebidas();
        LiveData<ArrayList<Bar>> networkData = mBarNetworkDataSource.getCurrentBar();
            networkData.observeForever((ArrayList<Bar> newBaresFromNetwork) -> {
                mExecutors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                            mBarDao.bulkInsert(newBaresFromNetwork);
                            Log.d(LOG_TAG, "Insertados nuevos bares en Room");
                    }
                });
            });
        LiveData<ArrayList<Bebida>> networkDataBebida = mBebidaNetworkDatasource.getCurrentBebida();
            networkDataBebida.observeForever((ArrayList<Bebida> newBebidasFromNetwork) ->{
                mExecutors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mBebidaDao.bulkInsert(newBebidasFromNetwork);
                        Log.d(LOG_TAG, "Insertados nuevas bebidas en Room");
                    }
                });
            });
    }
    public synchronized static BarRepository getInstance(BarDao barDao, BarNetworkDataSource barNetworkDataSource,BebidaDao bebidaDao ,BebidaNetworkDataSource bebidaNetworkDataSource,ReviewDao reviewDao){
        if(mInstance == null){
            mInstance = new BarRepository(barDao,barNetworkDataSource,bebidaDao,bebidaNetworkDataSource,reviewDao);
            Log.d(LOG_TAG, "Creado nuevo repositorio");
        }
        return mInstance;
    }

    private ArrayList<Bar> cargarBebidasEnBares(ArrayList<Bar> bares,ArrayList<Bebida> drinkList){
        for (Bar b: bares){
            for(Bebida drink : drinkList){
                if(!buscarNombreBebida((ArrayList<Bebida>) b.getBebidasBar(),drink.getNombre()))
                    b.getBebidasBar().add(drink);
            }
        }
        return bares;
    }
    private LiveData<ArrayList<Bar>> actualizarBebidas(ArrayList<Bar> baresAPI, ArrayList<Bar> baresDB, MutableLiveData<ArrayList<Bar>> bares){

        return bares;
    }
    public boolean buscarNombreBar(ArrayList<Bar> bares, String nombre){
        boolean success=false;
        int i = 0;
        Bar bar;
        while(!success&& i<bares.size()){
            bar=bares.get(i);
            i++;
            if(bar.getNombre().equals(nombre)){
                success=true;
            }
        }
        return success;
    }
    public boolean buscarNombreBebida(ArrayList<Bebida> bebidas, String nombre){
        boolean success=false;
        int i = 0;
        Bebida bebida;
        while(!success&& i<bebidas.size()){
            bebida=bebidas.get(i);
            i++;
            if(bebida.getNombre().equals(nombre)){
                success=true;
            }
        }
        return success;
    }
    public LiveData<List<Bar>> obtenerBaresActualizados(){
        return mBarDao.getAll();
    }
    public LiveData<List<Bar>> obtenerBaresFavoritosActualizados(){
        return mBarDao.getAllFav();
    }
    public void actualizarBar(Bar bar){mBarDao.updateStatus(bar);}
    public void insertarBar(Bar bar){mBarDao.insert(bar);}
    public Bar obtenerBar(String nombre){return mBarDao.getBar(nombre);}

    public LiveData<List<Review>> obtenerReviews(String nombre, Review.TipoReview tipo){
        return mReviewDao.getAllReviewsByType(nombre,tipo);
    }

    public void insertarReview(Review review){ mReviewDao.insert(review);}

    public LiveData<List<Bebida>> obtenerBebidasActualizadas(){ return mBebidaDao.getAll();}
    public LiveData<List<Bebida>> obtenerBebidasFavoritasActualizadas() {return mBebidaDao.getAllFav(); }
    public void actualizarBebida (Bebida bebida){mBebidaDao.updateStatus(bebida);}
}
