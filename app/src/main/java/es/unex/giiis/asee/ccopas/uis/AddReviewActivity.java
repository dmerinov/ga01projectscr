package es.unex.giiis.asee.ccopas.uis;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;
import java.util.Date;

import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.model.Review;

public class AddReviewActivity extends AppCompatActivity {
    private static String dateString;
    private EditText mAuthorText;
    private EditText mTitleText;
    private EditText mReviewText;
    private RatingBar mRatingBar;
    private static final int STARS_NUMBER = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_review_layout);

        //Obtain the views
        mAuthorText = findViewById(R.id.autor);
        mTitleText = findViewById(R.id.title);
        mReviewText = findViewById(R.id.text);
        mRatingBar = findViewById(R.id.ratingBar);
        mRatingBar.setNumStars(STARS_NUMBER);
        Date mDate = new Date();




        final Button cancelButton = findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent data = new Intent();
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        final Button resetButton = findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAuthorText.setText("");
                mTitleText.setText("");
                mReviewText.setText("");
                mRatingBar.setRating(0);
            }
        });

        final Button submitButton = findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Get author
                String mAuthorTextStr = mAuthorText.getText().toString();
                //Get title
                String mTitleTextStr = mTitleText.getText().toString();
                //Get text
                String mReviewTextStr = mReviewText.getText().toString();
                //Get rating
                float mRatingBarNum = mRatingBar.getRating();
                setDefaultDateTime(mDate);

                Intent data = new Intent();
                Review.packageIntent(data,mAuthorTextStr,mTitleTextStr,mReviewTextStr,mRatingBarNum,dateString);

                setResult(RESULT_OK,data);
                finish();
            }
        });

    }
    private void setDefaultDateTime(Date mDate) {

        mDate = new Date(mDate.getTime());

        Calendar c = Calendar.getInstance();
        c.setTime(mDate);
        setDateString(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH));
    }

    private static void setDateString(int year, int monthOfYear, int dayOfMonth) {

        // Increment monthOfYear for Calendar/Date -> Time Format setting
        monthOfYear++;
        String mon = "" + monthOfYear;
        String day = "" + dayOfMonth;

        if (monthOfYear < 10)
            mon = "0" + monthOfYear;
        if (dayOfMonth < 10)
            day = "0" + dayOfMonth;

        dateString = year + "-" + mon + "-" + day;
    }


}
