package es.unex.giiis.asee.ccopas.API;

import es.unex.giiis.asee.ccopas.model.BarOpenData;
import retrofit2.Call;
import retrofit2.http.GET;

public interface OpenDataService {
    @GET("http://opendata.caceres.es/GetData/GetData?dataset=om:BarCopas&format=json")
    Call<BarOpenData> listBar();
}

