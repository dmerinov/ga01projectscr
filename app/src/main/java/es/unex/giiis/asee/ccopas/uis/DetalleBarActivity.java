package es.unex.giiis.asee.ccopas.uis;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.ui.tabsDetalleBar.SectionsPagerAdapter;

public class DetalleBarActivity extends AppCompatActivity {

    SectionsPagerAdapter sectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_bar);
        Bar bar = (Bar) getIntent().getSerializableExtra("InstanciaBar");
        String nombreBar = getIntent().getStringExtra("Bar");
        manageTabs(bar);
    }
    private void manageTabs(Bar bar){
        sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(), bar);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }
}