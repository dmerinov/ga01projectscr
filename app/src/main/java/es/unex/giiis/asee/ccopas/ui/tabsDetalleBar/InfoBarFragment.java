package es.unex.giiis.asee.ccopas.ui.tabsDetalleBar;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.Random;

import es.unex.giiis.asee.ccopas.API.BarNetworkLoaderRunnable;
import es.unex.giiis.asee.ccopas.BarNetworkDataSource;
import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.BebidaNetworkDataSource;
import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.InjectorUtils;
import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;
import es.unex.giiis.asee.ccopas.uis.viewmodels.InfoBarFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.InfoBarFragmentViewModelFactory;

public class InfoBarFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String BAR_LIST = "barList";
    private String nombreBar;
    private Bar bar;
    private InfoBarFragmentViewModel mViewModel;
    private Random random = new Random();


    public static InfoBarFragment newInstance(int index, Bar bar) {
        InfoBarFragment fragment = new InfoBarFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);


        if(bar != null) {
            bundle.putSerializable(BAR_LIST, bar);
        }

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewModelProvider.NewInstanceFactory view = new ViewModelProvider.NewInstanceFactory();
        view.create(PageViewModel.class);
        PageViewModel pageViewModel = new ViewModelProvider(this, view).get(PageViewModel.class);
        InfoBarFragmentViewModelFactory factory = InjectorUtils.provideInfoBarFragmentViewModelFactory(this.getActivity().getApplicationContext());
        mViewModel = new ViewModelProvider(this,factory).get(InfoBarFragmentViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            bar = (Bar) getArguments().getSerializable(BAR_LIST);
        }


        pageViewModel.setIndex(index);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View root= inflater.inflate(R.layout.fragment_detalle_bar, container, false);
        Context context = getContext();

        TextView aforoMaxBar, localizacion, nombre,aforoActual;

        aforoMaxBar = root.findViewById(R.id.tvDatosAforoMax);
        aforoActual = root.findViewById(R.id.tvDatosAforo);
        String randomNum;

        Button masInfo = root.findViewById(R.id.buttonSaberMas);
        masInfo.setOnClickListener(view -> {
            String busqueda = "https://www.google.com/search?q="+bar.getNombre()+"%20Caceres";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(busqueda));
            startActivity(i);
        });

        ImageButton btLlamar= root.findViewById(R.id.btLlamar);
        funcionLLamar(btLlamar,bar.getTelefono());


        int aforoMax = Integer.parseInt(bar.getAforo());

        if ((bar.getAforo().equals("0"))) {
            aforoMaxBar.setText(getString(R.string.sin_capacidad));
            randomNum = String.valueOf(random.nextInt(60));
        } else {
            aforoMaxBar.setText(bar.getAforo());
            randomNum = String.valueOf(random.nextInt(aforoMax));
        }
        aforoActual.setText(randomNum);

        localizacion = root.findViewById(R.id.tvDatosLocalizacion);
        localizacion.setText(bar.getDireccion());
        nombre = root.findViewById(R.id.tvNombreBar);
        nombre.setText(bar.getNombre());

        ImageView fotoBar;
        fotoBar = root.findViewById(R.id.imageBar);
        fotoBar.setImageResource(bar.getMain_photo());
        ImageButton btCompartir= root.findViewById(R.id.btCompartir);
        funcionCompartir(btCompartir,bar.getNombre()+"\n"+bar.getDireccion(),root);
        int aforoAct=Integer.parseInt((String) aforoActual.getText());
        TextView ocupacion=  root.findViewById(R.id.tvOcupacion);
        comprobarAforo(aforoMax,aforoAct,ocupacion);

        CheckBox favBar;
        favBar = root.findViewById(R.id.cbFavBar);
        favBar.setChecked(bar.isFav());
        favBar.setOnCheckedChangeListener((buttonView, isChecked) -> {
            // is called when the user toggles the status checkbox
            AppExecutors.getInstance().diskIO().execute(()-> {
                bar.setFav(isChecked);
                mViewModel.actualizarBar(bar);
            });
        });

        return root;
    }

    private void comprobarAforo(int aforoMax, int aforoActual,TextView ocupacion) {

        if(aforoActual == 0){ //Si el aforo esta a 0
            ocupacion.setText(R.string.vacio);
        }
        if (aforoActual < aforoMax/2){//Si el aforo actual estaa por debajo la mitad
            ocupacion.setText(R.string.casivacio);
        }
        if (aforoActual > aforoMax/2){//Si el aforo actual estaa por encima la mitad
            ocupacion.setText(R.string.casiLLeno);
        }
        if (aforoActual == aforoMax){
            ocupacion.setText(R.string.lleno);
        }
        if(aforoMax == 0){
            ocupacion.setText(R.string.sinAforo);
        }
    }
    private void funcionCompartir(ImageButton btCompartir, String msg, View v) {
        final String appName = "org.telegram.messenger";
        final boolean isAppInstalled = isAppAvailable(v.getContext().getApplicationContext(), appName);

        btCompartir.setOnClickListener(view -> {
            if (isAppInstalled)
            {
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                myIntent.setPackage(appName);
                myIntent.putExtra(Intent.EXTRA_TEXT, msg);//
                startActivity(Intent.createChooser(myIntent, "Share with"));
            }else
            {
                Toast.makeText(v.getContext(), "Telegram not Installed", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public static boolean isAppAvailable(Context context, String appName)
    {
        PackageManager pm = context.getPackageManager();
        try
        {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            return false;
        }
    }
    private void funcionLLamar(ImageButton btLlamar, String telefono) {
        String uri = "tel:" + telefono.trim() ;

        btLlamar.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            startActivity(intent);
        });

    }

}
