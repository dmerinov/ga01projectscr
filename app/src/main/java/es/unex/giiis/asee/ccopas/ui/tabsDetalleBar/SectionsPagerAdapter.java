package es.unex.giiis.asee.ccopas.ui.tabsDetalleBar;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.R;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3};
    private final Fragment[] tabs;
    private final Context mContext;
    private Bar bar;

    public SectionsPagerAdapter(Context context, FragmentManager fm, Bar bar) {
        super(fm);
        mContext = context;
        tabs = new Fragment[3];
        this.bar = bar;
    }

    /**
     * Is called to instantiate the fragment for the given page
     *
     * @param position
     * @return a PlaceholderFragment (defined as a static inner class below)
     */
    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                tabs[position] = InfoBarFragment.newInstance(position, bar);
                break;
            case 1:
                tabs[position] = CartaBarFragment.newInstance(position, bar);
                break;
            case 2:
                tabs[position] = ReviewFragment.newInstance(position, bar);
                break;
        }
        return tabs[position];
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return TAB_TITLES.length;
    }
}