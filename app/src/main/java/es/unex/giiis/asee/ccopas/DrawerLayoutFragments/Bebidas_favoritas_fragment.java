package es.unex.giiis.asee.ccopas.DrawerLayoutFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import es.unex.giiis.asee.ccopas.BarNetworkDataSource;
import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.BebidaNetworkDataSource;
import es.unex.giiis.asee.ccopas.InjectorUtils;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.uis.DetalleBebidaActivity;
import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.adapters.bebidasAdapter;
import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BarListFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BarListFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BebidasFavoritasFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BebidasFavoritasFragmentViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BarListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Bebidas_favoritas_fragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView bebidasListRecyclerView;
    private RecyclerView.LayoutManager barLayoutManager;
    private bebidasAdapter bebidasListAdapter;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private BebidasFavoritasFragmentViewModel mViewModel;

    public Bebidas_favoritas_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment BarListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Bebidas_favoritas_fragment newInstance(String param1) {
        Bebidas_favoritas_fragment fragment = new Bebidas_favoritas_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.content_bar_manager, container, false);
            BebidasFavoritasFragmentViewModelFactory factory = InjectorUtils.provideBebidasFavoritasFragmentViewModelFactory(this.getActivity().getApplicationContext());
            mViewModel = new ViewModelProvider(this,factory).get(BebidasFavoritasFragmentViewModel.class);
            bebidasListRecyclerView = v.findViewById(R.id.bar_reclycler_view);
            bebidasListRecyclerView.setHasFixedSize(true);
            bebidasListRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
            bebidasListAdapter = new bebidasAdapter(v.getContext(),getActivity(), new bebidasAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Bebida bebida) {
                    Context context = v.getContext().getApplicationContext();
                    CharSequence text = "bebida " + bebida.getNombre() + " pulsada";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    Intent irBebida = new Intent(context, DetalleBebidaActivity.class);
                    irBebida.putExtra("InstanciaBebida", bebida);
                    startActivity(irBebida);
                    }
                });
            bebidasListRecyclerView.setAdapter(bebidasListAdapter);

            mViewModel.getmBebidas().observe(getViewLifecycleOwner(), bebidas -> onBebidaLoaded((ArrayList<Bebida>) bebidas));
            return v;
    }

    private void onBebidaLoaded(ArrayList<Bebida> bebidas) {
        getActivity().runOnUiThread(() -> bebidasListAdapter.addAll(bebidas));
    }
}
