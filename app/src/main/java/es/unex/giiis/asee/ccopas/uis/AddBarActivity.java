package es.unex.giiis.asee.ccopas.uis;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.model.Bar;

public class AddBarActivity extends AppCompatActivity {

    private EditText mNombreBar;
    private EditText mDireccion;
    private EditText mAforo;
    private EditText mTelefono;
    private Button submit;

    public AddBarActivity() {
        // Required empty public constructor
    }

    public static AddBarActivity newInstance() {
        AddBarActivity fragment = new AddBarActivity();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bar);

        mNombreBar = findViewById(R.id.tv_nombreNewBar);
        mDireccion = findViewById(R.id.tv_direccionNewBar);
        mAforo = findViewById(R.id.tv_aforoMax);
        mTelefono = findViewById(R.id.tv_telefonoNewBar);

        submit=findViewById(R.id.btnAddBar);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mNombreBar.length() == 0 || mDireccion.length() == 0 || mAforo.length() == 0 || mTelefono.length() == 0){
                    Toast toast = Toast.makeText(getApplicationContext(), "Hay campos vacios", Toast.LENGTH_SHORT);
                    toast.show();
                }else{
                    Intent data = new Intent();
                    Bar.packageIntent(data,mNombreBar.getText().toString(),mDireccion.getText().toString(),mAforo.getText().toString(),mTelefono.getText().toString());

                    setResult(RESULT_OK,data);
                    finish();
                }
            }
        });
    }


}