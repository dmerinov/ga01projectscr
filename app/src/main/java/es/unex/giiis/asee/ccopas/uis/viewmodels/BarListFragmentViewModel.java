package es.unex.giiis.asee.ccopas.uis.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.model.Bar;

public class BarListFragmentViewModel extends ViewModel {
    private final BarRepository barRepository;
    private final LiveData<List<Bar>> mBares;

    public BarListFragmentViewModel(BarRepository barRepository){
        this.barRepository = barRepository;
        mBares= barRepository.obtenerBaresActualizados();
    }

    public LiveData<List<Bar>> getBares(){
        return mBares;
    }
    public void insertarBar(Bar bar){
        barRepository.insertarBar(bar);
    }
}
