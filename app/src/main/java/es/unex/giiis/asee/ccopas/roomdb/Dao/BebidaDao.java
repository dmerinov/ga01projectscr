package es.unex.giiis.asee.ccopas.roomdb.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.model.Bebida;

import static androidx.room.OnConflictStrategy.IGNORE;


@Dao
public interface BebidaDao {
    @Query("SELECT * FROM bebidas")
    public LiveData<List<Bebida>> getAll();

    @Query("SELECT * FROM bebidas WHERE fav = 1")
    public LiveData<List<Bebida>> getAllFav();

    @Query("SELECT * FROM bebidas WHERE nombre=:nombre")
    public Bebida getBebida(String nombre);

    @Insert (onConflict = IGNORE)
    public long insert(Bebida bebida);

    @Insert(onConflict = IGNORE)
    void bulkInsert(ArrayList<Bebida> bebida);

    @Query("DELETE FROM bebidas")
    public void deleteAll();

    @Update
    public int updateStatus(Bebida bebida);

}
