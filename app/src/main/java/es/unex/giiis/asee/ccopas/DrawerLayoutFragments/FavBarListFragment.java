package es.unex.giiis.asee.ccopas.DrawerLayoutFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import es.unex.giiis.asee.ccopas.BarNetworkDataSource;
import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.BebidaNetworkDataSource;
import es.unex.giiis.asee.ccopas.InjectorUtils;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.uis.DetalleBarActivity;
import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.adapters.BarListAdapter;
import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BarListFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BarListFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.FavBarListFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.FavBarListFragmentViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavBarListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavBarListFragment extends Fragment{

    private static final String ARG_PARAM1 = "param1";

    private RecyclerView favBarListRecyclerView;
    private BarListAdapter barListAdapter;
    private FavBarListFragmentViewModel mViewModel;


    public FavBarListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param title Parameter 1.
     * @return A new instance of fragment FavBarListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavBarListFragment newInstance(String title) {
        FavBarListFragment fragment = new FavBarListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, title);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v= inflater.inflate(R.layout.content_bar_manager, container, false);

        FavBarListFragmentViewModelFactory factory = InjectorUtils.provideFavBarListFragmentViewModelFactory(this.getActivity().getApplicationContext());
        mViewModel = new ViewModelProvider(this,factory).get(FavBarListFragmentViewModel.class);

        FloatingActionButton add_bar =  v.findViewById(R.id.add_bar_fab);
        add_bar.setVisibility(View.INVISIBLE);

        favBarListRecyclerView = v.findViewById(R.id.bar_reclycler_view);
        favBarListRecyclerView.setHasFixedSize(true);
        favBarListRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        barListAdapter=new BarListAdapter(v.getContext(), new BarListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Bar bar) {
                Context context = v.getContext();
                CharSequence text = "Bar " + bar.getNombre() + " pulsado";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                Intent infoBarIntent = new Intent(context, DetalleBarActivity.class);
                infoBarIntent.putExtra("InstanciaBar", bar);
                startActivity(infoBarIntent);
            }
        });
        favBarListRecyclerView.setAdapter(barListAdapter);

        mViewModel.getBares().observe(getViewLifecycleOwner(), bares -> onBarLoaded((ArrayList<Bar>) bares));


        return v;
    }

        public void onBarLoaded(ArrayList<Bar> bares){
            getActivity().runOnUiThread(() -> barListAdapter.addAll(bares));
        }

}