package es.unex.giiis.asee.ccopas.API;

import es.unex.giiis.asee.ccopas.model.BarOpenData;

public interface OnBarLoadedListener {
    public void onBarLoaded(BarOpenData baresOpenData);
}
