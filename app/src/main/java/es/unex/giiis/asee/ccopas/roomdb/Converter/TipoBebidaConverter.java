package es.unex.giiis.asee.ccopas.roomdb.Converter;

import androidx.room.TypeConverter;

import es.unex.giiis.asee.ccopas.model.Bebida;

public class TipoBebidaConverter {
    @TypeConverter
    public String tipoBebidaToString(Bebida.TipoBebida tipo){
        return tipo == null ? null : tipo.name();
    }
    @TypeConverter
    public Bebida.TipoBebida stringTotipoBebida (String tipo){
        return  tipo == null ? null : Bebida.TipoBebida.valueOf(tipo);
    }
}
