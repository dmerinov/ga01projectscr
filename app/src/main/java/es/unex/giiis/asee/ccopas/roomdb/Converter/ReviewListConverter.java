package es.unex.giiis.asee.ccopas.roomdb.Converter;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.ccopas.model.Review;

public class ReviewListConverter {
    @TypeConverter
    public List<Review> toReviewList(String reviewListStr){
        if(reviewListStr==null){
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Review>>(){
        }.getType();
        List<Review> reviewList = gson.fromJson(reviewListStr,type);
        return reviewList;
    }
    @TypeConverter
    public String fromReviewList (List<Review> reviewList){
        if (reviewList == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Review>>() {
        }.getType();
        String json = gson.toJson(reviewList, type);
        return json;
    }
}
