package es.unex.giiis.asee.ccopas.API;

import java.io.IOException;

import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.model.BarOpenData;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BarNetworkLoaderRunnable implements Runnable{

    private final OnBarLoadedListener mOnBarLoadedListener;

    public BarNetworkLoaderRunnable(OnBarLoadedListener onReposLoadedListener){
        mOnBarLoadedListener = onReposLoadedListener;
    }

    @Override
    public void run() {
        // Instanciación de Retrofit y llamada síncrona
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://opendata.caceres.es")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenDataService service = retrofit.create(OpenDataService.class);

        try {
            BarOpenData baresOP = service.listBar().execute().body();
            AppExecutors.getInstance().mainThread().execute(() -> mOnBarLoadedListener.onBarLoaded(baresOP));
        } catch (IOException e) {
        }
    }
}

