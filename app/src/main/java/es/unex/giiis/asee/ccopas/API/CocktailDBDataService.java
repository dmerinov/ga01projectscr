package es.unex.giiis.asee.ccopas.API;

import es.unex.giiis.asee.ccopas.model.cocktailAPI.CocktailDB;
import retrofit2.Call;
import retrofit2.http.GET;

public interface CocktailDBDataService {
    @GET("https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic")
    Call<CocktailDB> listBebida();
}
