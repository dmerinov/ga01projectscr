package es.unex.giiis.asee.ccopas.DetalleBebidas;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

import es.unex.giiis.asee.ccopas.InjectorUtils;
import es.unex.giiis.asee.ccopas.uis.AddReviewActivity;
import es.unex.giiis.asee.ccopas.BarNetworkDataSource;
import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.BebidaNetworkDataSource;
import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.adapters.ReviewListAdapter;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.model.Review;
import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;
import es.unex.giiis.asee.ccopas.ui.tabsDetalleBar.PageViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.CartaBarFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.CartaBarFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.PlaceholderBebidasFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.PlaceholderBebidasFragmentViewModelFactory;

import static android.app.Activity.RESULT_OK;


public class PlaceholderBebidasFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String BEBIDA = "bebida";
    private static final int ADD_TODO_ITEM_REQUEST = 0;
    private Bebida bebida;
    private PageViewModel pageViewModel;
    private ReviewListAdapter reviewListAdapter;
    private RecyclerView reviewBebidaRecyclerView;
    private PlaceholderBebidasFragmentViewModel mViewModel;


    public static PlaceholderBebidasFragment newInstance(int index, Bebida bebida) {
        PlaceholderBebidasFragment fragment = new PlaceholderBebidasFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER,index);


        if(bebida != null){
            bundle.putSerializable(BEBIDA,bebida);
        }

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewModelProvider.NewInstanceFactory view = new ViewModelProvider.NewInstanceFactory();
        view.create(PageViewModel.class);


        pageViewModel= new ViewModelProvider(this, view).get(PageViewModel.class);

        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            bebida = (Bebida) getArguments().getSerializable(BEBIDA);
        }
        pageViewModel.setIndex(index);
    }
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        Context context = getContext();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        View root = inflater.inflate(R.layout.activity_detalle_bebida, container, false);
        int index = -1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            bebida = (Bebida) getArguments().getSerializable(BEBIDA);
        }
        PlaceholderBebidasFragmentViewModelFactory factory = InjectorUtils.providePlaceholderBebidasFragmentViewModelFactory(this.getActivity().getApplicationContext(),bebida.getNombre());
        mViewModel = new ViewModelProvider(this,factory).get(PlaceholderBebidasFragmentViewModel.class);

        switch (index){
            case 0:
                root = inflater.inflate(R.layout.fragment_detalle_bebida,container,false);
                TextView nombre,precio,tipoBebida;
                CheckBox favBebida = root.findViewById(R.id.checkBoxBebidaFav);
                favBebida.setChecked(bebida.isFav());
                favBebida.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        // is called when the user toggles the status checkbox
                        AppExecutors.getInstance().diskIO().execute(()-> {
                            bebida.setFav(isChecked);
                            mViewModel.actualizarBebida(bebida);
                        });
                    }
                });

                nombre=root.findViewById(R.id.tvNombreBebida);
                nombre.setText(bebida.getNombre());

                precio=root.findViewById(R.id.tvPrecio);
                int precioRandom= new Random().nextInt(10);
                if(precioRandom <4)
                    precioRandom=4;

                precio.setText(String.valueOf(precioRandom+" €"));

                tipoBebida=root.findViewById(R.id.tvTipoBebida);
                //tipoBebida.setText(bebida.getTypeDrink());
                tipoBebida.setText("Alcohólica");

                ImageView imageDrink= root.findViewById(R.id.ivImagen);
                Uri uri= Uri.parse(bebida.getURL());
                Picasso.get().load(uri).placeholder(R.drawable.copa).into(imageDrink);
                break;
            case 1:
                root = inflater.inflate(R.layout.fragment_review_bar,container,false);

                reviewBebidaRecyclerView = root.findViewById(R.id.bar_review_recycler_view);
                reviewBebidaRecyclerView.setHasFixedSize(true);
                reviewBebidaRecyclerView.addItemDecoration(new DividerItemDecoration(root.getContext(), DividerItemDecoration.VERTICAL));
                reviewBebidaRecyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

                reviewListAdapter = new ReviewListAdapter(root.getContext(), new ReviewListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Review review) {

                    }
                });

                FloatingActionButton fab =  root.findViewById(R.id.add_review_fab);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getActivity().getApplicationContext();
                        Intent addReviewIntent = new Intent(view.getContext(), AddReviewActivity.class);
                        startActivityForResult(addReviewIntent,ADD_TODO_ITEM_REQUEST);
                    }
                });
                reviewBebidaRecyclerView.setAdapter(reviewListAdapter);

                mViewModel.getmReviews().observe(getViewLifecycleOwner(), reviews -> onReviewsLoaded((ArrayList<Review>)reviews));

                break;
        }

        return root;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if(requestCode==ADD_TODO_ITEM_REQUEST){
            if(resultCode==RESULT_OK){
                Review review = new Review(data,bebida.getNombre());
                review.setTipo(Review.TipoReview.Bebida);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mViewModel.insertarReview(review);
                    }
                });

            }
        }
    }
    private void onReviewsLoaded(ArrayList<Review> reviews) {
        getActivity().runOnUiThread(() -> reviewListAdapter.addAll(reviews));
    }
}
