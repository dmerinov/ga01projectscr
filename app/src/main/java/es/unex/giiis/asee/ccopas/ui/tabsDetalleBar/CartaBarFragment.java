package es.unex.giiis.asee.ccopas.ui.tabsDetalleBar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import es.unex.giiis.asee.ccopas.BarNetworkDataSource;
import es.unex.giiis.asee.ccopas.BarRepository;
import es.unex.giiis.asee.ccopas.BebidaNetworkDataSource;
import es.unex.giiis.asee.ccopas.InjectorUtils;
import es.unex.giiis.asee.ccopas.uis.DetalleBebidaActivity;
import es.unex.giiis.asee.ccopas.Executors.AppExecutors;
import es.unex.giiis.asee.ccopas.R;
import es.unex.giiis.asee.ccopas.adapters.bebidasAdapter;
import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.Bebida;
import es.unex.giiis.asee.ccopas.roomdb.BarDatabase;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BebidasFavoritasFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.BebidasFavoritasFragmentViewModelFactory;
import es.unex.giiis.asee.ccopas.uis.viewmodels.CartaBarFragmentViewModel;
import es.unex.giiis.asee.ccopas.uis.viewmodels.CartaBarFragmentViewModelFactory;

public class CartaBarFragment extends Fragment implements SearchView.OnQueryTextListener {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String BAR_LIST = "barList";
    private static final String BEBIDAS_LIST = "bebidasList" ;
    private RecyclerView listaBebidasRV;
    private String nombreBar;
    private ArrayList<Bebida> listBebidas = new ArrayList<>();
    private bebidasAdapter bebidasAdapter;
    private CartaBarFragmentViewModel mViewModel;


    public static CartaBarFragment newInstance(int index, Bar bar) {
        CartaBarFragment fragment = new CartaBarFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);


        if(bar != null) {
            bundle.putSerializable(BAR_LIST, bar);
        }

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listBebidas=new ArrayList<>();
        ViewModelProvider.NewInstanceFactory view = new ViewModelProvider.NewInstanceFactory();
        view.create(PageViewModel.class);
        PageViewModel pageViewModel = new ViewModelProvider(this, view).get(PageViewModel.class);

        CartaBarFragmentViewModelFactory factory = InjectorUtils.provideCartaBarFragmentViewModelFactory(this.getActivity().getApplicationContext());
        mViewModel = new ViewModelProvider(this,factory).get(CartaBarFragmentViewModel.class);

        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
            Bar bar = (Bar) getArguments().getSerializable(BAR_LIST);
            if(bar != null)
                this.listBebidas.addAll(bar.getBebidasBar());
        }

        pageViewModel.setIndex(index);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View root= inflater.inflate(R.layout.fragment_carta_bar, container, false);
        listaBebidasRV = root.findViewById(R.id.listaCartaBarRV);
        Context context = root.getContext();

        bebidasAdapter = new bebidasAdapter(this.getContext(), getActivity(), bebida -> {
            CharSequence text = "Bebida " + bebida.getNombre() + " pulsado";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

            Intent irBebida = new Intent(context, DetalleBebidaActivity.class);
            irBebida.putExtra("InstanciaBebida", bebida);
            startActivity(irBebida);
        });
        LinearLayoutManager LLManager = new LinearLayoutManager(this.getContext());
        LLManager.setOrientation(LinearLayoutManager.VERTICAL);
        listaBebidasRV.setLayoutManager(LLManager);
        listaBebidasRV.setAdapter(bebidasAdapter);
        SearchView svBuscarBebida= root.findViewById(R.id.svBebidas);
        svBuscarBebida.setQueryHint("Buscar bebida");
        svBuscarBebida.setOnQueryTextListener(this);
        svBuscarBebida.setOnCloseListener(() -> {
            bebidasAdapter.setFilters(listBebidas);
            return true;
        });

        mViewModel.getmBebidas().observe(getViewLifecycleOwner(), bebidas -> onBebidaLoaded((ArrayList<Bebida>) bebidas));

        return root;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        ArrayList<Bebida> listBebidasBuscar;
        if (s.length() >0){
            listBebidasBuscar=this.filter(s,listBebidas);
            if (listBebidasBuscar.size() > 0){
                bebidasAdapter.setFilters(listBebidasBuscar);
            }
        }else{
            if (bebidasAdapter != null){
                bebidasAdapter.setFilters(listBebidas);
            }
        }

        return true;
    }


    public ArrayList<Bebida> filter(String nombreBuscar, ArrayList<Bebida> listBebidasBuscar){
        ArrayList<Bebida> BebidasEncontradas= new ArrayList<>();
        String nombreBebida;
        nombreBuscar=nombreBuscar.toLowerCase();

        for (Bebida bebida:listBebidasBuscar){
            nombreBebida=bebida.getNombre().toLowerCase();
            if(nombreBebida.contains(nombreBuscar)){
                BebidasEncontradas.add(bebida);
            }
        }
        return BebidasEncontradas;
    }
    private void onBebidaLoaded(ArrayList<Bebida> bebidas) {
        getActivity().runOnUiThread(() -> bebidasAdapter.addAll(bebidas));
    }

    public ArrayList<Bebida> getListBebidas() {
        return listBebidas;
    }
}
