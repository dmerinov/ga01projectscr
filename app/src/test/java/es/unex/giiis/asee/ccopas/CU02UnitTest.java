package es.unex.giiis.asee.ccopas;

import org.junit.Test;

import java.util.Date;

import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.Review;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CU02UnitTest {
    @Test
    public void PonerResenaBarTest(){

        Date mockDate = new Date(2020,6, 20);
        Bar mockBar = new Bar("nombreBarMock", "111222333", "direccionF", 1, false, "20");
        Review mockReview = new Review(1,mockBar.getNombre(), "user", "title", "text", mockDate, 3);
        assertEquals(mockBar.getBarReviews().size(),0);
        mockBar.getBarReviews().add(mockReview);
        assertEquals(mockBar.getBarReviews().size(),1);
        assertEquals(mockBar.getBarReviews().get(0).getText(),"text");
        assertEquals(mockBar.getBarReviews().get(0).getTitle(),"title");
        assertEquals(mockBar.getBarReviews().get(0).getRate(),3.0,0);
        assertEquals(mockBar.getBarReviews().get(0).getReviewId(),1);
        assertEquals(mockBar.getBarReviews().get(0).getUser(),"user");
        assertEquals(mockBar.getBarReviews().get(0).getDate(),mockDate);

    }
}