package es.unex.giiis.asee.ccopas;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import es.unex.giiis.asee.ccopas.DrawerLayoutFragments.BarListFragment;
import es.unex.giiis.asee.ccopas.model.Bar;

public class CU14UnitTest {

    private BarListFragment BLF = new BarListFragment();
    private ArrayList<Bar> ListaBares = new ArrayList<>();
    @Test
    public void aniadirBarTes(){

        ListaBares=BLF.getBares();
        Assert.assertEquals(ListaBares.size(),0);
        Bar mockBar = new Bar("nombreBarMock", "111222333", "direccionF", 1, false, "20");
        ListaBares.add(mockBar);
        Assert.assertEquals(ListaBares.size(),1);

    }

}
