package es.unex.giiis.asee.ccopas;

import org.junit.Test;

import es.unex.giiis.asee.ccopas.model.Bar;
import es.unex.giiis.asee.ccopas.model.Bebida;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CU09UnitTest {

    @Test
    public void initTest(){
        Bar mockBar = new Bar("nombreBarMock", "111222333", "direccionF", 1, false, "20");
        Bebida bebidaMock = new Bebida("BebidaMock","URL");
        Bebida bebidaMock2 = new Bebida("BebidaMock2","URL");

        mockBar.getBebidasBar().add(bebidaMock);
        mockBar.getBebidasBar().add(bebidaMock2);

        assertNotEquals(mockBar.getBebidasBar().size(),0);
        assertEquals(mockBar.getBebidasBar().get(0).getNombre(),"BebidaMock");
        assertEquals(mockBar.getBebidasBar().get(1).getNombre(),"BebidaMock2");
    }
}
