package es.unex.giiis.asee.ccopas;

import android.os.Bundle;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;


import es.unex.giiis.asee.ccopas.DrawerLayoutFragments.BarListFragment;
import es.unex.giiis.asee.ccopas.model.Bar;

import static org.junit.Assert.assertEquals;


public class ListarBaresTest {
    private BarListFragment fragment;
    @Test
    public void comprobarListaBares(){
        Bar bar = new Bar("Hola","121212122","Direccion",R.drawable.bar_icon,false,"64");
        ArrayList<Bar> bares = new ArrayList<>();
        bares.add(bar);
        fragment = new BarListFragment();
        fragment.getBares().add(bar);
        assertEquals(fragment.getBares().size(),1);
    }
}
